/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, TouchableHighlight, TouchableOpacity, TextInput,Image } from 'react-native';



const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

  state = {
    username: '',
    password: ''
} 

  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.content, styles.center]}>
          <View style={[styles.image, styles.center]}>
            <Image style={styles.image} source={{ uri: 'https://www.dictionary.com/e/wp-content/uploads/2018/05/BTS-300x300.jpg' }} />
          </View>
        </View>


        <View style={styles.content}>
          <Text style={{alignItems: 'center'}}>{this.state.username} - {this.state.password}</Text>
          <View>
            <TextInput onChangeText={(value) => {this.setState({username:value})}} value={this.state.username} style={styles.textInput} placeholder='Username'/>
          </View>

          <View>
            <TextInput onChangeText={(value) => {this.setState({password:value})}} value={this.state.password} style={styles.textInput} placeholder='Password'/>
          </View>


          <TouchableOpacity onPress={() => { Alert.alert('touchhhhhhh') }}>
            <View style={styles.touchable}>
              <Text>touchableOpacity</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey'
  },
  image: {
    alignItems: 'center',
    backgroundColor: 'white',
    width: 200,
    height: 200,
    borderRadius: 100

  },
  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  textInput: {
    margin: 8,
    alignItems: 'center',
    backgroundColor: 'white',
    height: 50
  },
  touchable: {
    alignItems: 'center',
    padding: 20,
    backgroundColor: 'darkgrey',
    margin: 14,

  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  }


});