import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, TouchableHighlight, TouchableOpacity, TextInput, Image, Modal } from 'react-native';

export default class App1 extends Component {

    state = {
        modalVisible: false
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>

                </View>

                <View style={styles.content}>
                    <Modal
                        transparent = {true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            Alert.alert('Modal has been closed.');
                        }}
                        >
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({modalVisible:true})
                            }}>
                            <Image source={require('./BTS-300x300.jpg')}/>
                        </TouchableOpacity>
                        
                    </Modal>
                    
                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

})